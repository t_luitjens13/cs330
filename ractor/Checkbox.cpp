#include "Checkbox.h"

bool Checkbox::IsChecked(){
	return checked;
}

void Checkbox::IsChecked(bool check){
	checked = check;
}

void Checkbox::Draw(){
	if(checked)
		mvprintw(y,x,("[X] " + text).c_str());
	else
		mvprintw(y,x, ("[ ] "+text).c_str());
}

void Checkbox::AddListener(function<void(bool)> listener){
	listeners.push_back(listener);
}

void Checkbox::OnMouseClick(int x, int y){
	IsChecked(!checked); //change state
	for(function<void(bool)> listener : listeners){
		listener(checked);
	}	
}
