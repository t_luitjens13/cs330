#include "Widget.h"
using namespace std;

Widget::Widget(int lft, int rght, int tp, int bttm){
	
	left = lft;
	right = rght;
	top = tp;
	bottom = bttm;
}
Widget::~Widget(){
}
int Widget:: GetLeft() const{
	return left;
}

int Widget::GetRight() const{
	return right;
}

int Widget::GetTop() const{
	return top;
}

int Widget::GetBottom() const{
	return bottom;
}
void Widget::SetRight(int x){
	right = x;
}
void Widget::SetLeft(int x){
	left = x;
}
void Widget::SetTop(int x){
	top = x;
}
void Widget::SetBottom(int x){
	bottom = x;
}
bool Widget::Contains(int x,int y) const{
	if(x <= right && x >= left && y >= top && y <= bottom)
		return true;
	return false;
}
void Widget::OnMouseClick(int x, int y){

}
void Widget::Draw(){

}


