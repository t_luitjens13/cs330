#include "Window.h"

void Window::Loop(){ 
	int x=0;
	int y=0;	
	while(!stop){
		
		Draw();
		int input = getch();
		if( (char)input == 'q'){
			Stop();
		}
		else if (input == KEY_MOUSE){
			getmouse(&event);
			
			x=event.x;
			y=event.y;
			 
			for(Widget* widget : widgetList){
				if(widget->Contains(x,y)){
					widget->OnMouseClick(x-widget->GetLeft(),y-widget->GetTop()); //2nd param may need to be bot-y
				}
				
							
			}
		}
		refresh();
	}
	endwin();
}

void Window::Add(Widget* widget){
	widgetList.push_back(widget);
}

void Window::Draw(){
	clear();
	for(Widget* widget : widgetList){
		widget->Draw();
	}
	
}

void Window::Stop(){
	stop = true;
}

/* 
	no matter where the screen is clicked, the slider is incremented to the next value
		only happens when slider is added last
		otherwise, simply nothing gets clicked or happens		
*/




