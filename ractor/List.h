#include "Widget.h"
#include <functional>
#include <vector>
#ifndef LIST_H
#define LIST_H
using namespace std;

class List : public Widget{
	private:
		int x;
		int y;
		vector<string> options;
		vector<function<void(int)>> listeners;
		int index;
	public:
		List(int xCoord, int yCoord, const vector<string>& inoptions) : Widget(0,0,0,0){
			x = xCoord;
			y = yCoord;
			options = inoptions;
			//width needs to be the longest option length -1
			//height needs to be number of options -1 
			int right = 0;
			for(string str : inoptions){
				if(str.length() > right)
					right = str.length();
			}
			Widget::SetLeft(xCoord);
			Widget::SetTop(yCoord);
			Widget::SetBottom(yCoord+inoptions.size()-1);
			Widget::SetRight(right-1+xCoord);
			index = -1;
		}
		int GetSelectedIndex() const;
		const string& GetSelected() const;
		void Draw();
		void AddListener(function<void(int)> listener);
		void OnMouseClick(int x, int y);
};
#endif
