#include "Button.h"

void Button::Draw(){
	attron(A_REVERSE);
	mvprintw(y,x,text.c_str()); //once again not quite sure if this is the correct formatting for this
	attroff(A_REVERSE);
}

void Button::AddListener(function<void()> listener){
	listeners.push_back(listener);
}

void Button::OnMouseClick(int x, int y){
	for(function<void()> listener : listeners){
		listener();
	}
}	
