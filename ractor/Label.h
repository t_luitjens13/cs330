#include "Widget.h"
#ifndef LABEL_H
#define LABEL_H
using namespace std;
class Label : public Widget{
	private:
		int x;
		int y;
		int bound;
		string text;
	public:
		Label(int xCoord, int yCoord, const string& intext) : Widget(0,0,0,0){
			x = xCoord;
			y = yCoord;
			text = intext;
			Widget::SetLeft(xCoord);
			Widget::SetTop(yCoord);
			Widget::SetBottom(yCoord);
			Widget::SetRight(intext.length()-1+xCoord);
		}
		void Draw();
		void SetText(const string& text);
};
#endif //LABEL_H
