#include "Widget.h"
#include <functional>
#include <vector>
#ifndef SLIDER_H
#define SLIDER_H
using namespace std;

class Slider : public Widget{
	private:
		int x;
		int y;
		int max;
		int defaultVal;
		int currentVal;
		vector<function<void(int)>> listeners;
	public:
		Slider(int xCoord, int yCoord, int maxVal, int defaultValue) : Widget(0,0,0,0){
			x = xCoord;
			y = yCoord;
			max = maxVal;
			defaultVal = defaultValue;
			currentVal = defaultValue;
			Widget::SetLeft(x);
			Widget::SetRight(x+max);
			Widget::SetTop(y);
			Widget::SetBottom(y);
		}
		int GetValue() const;
		int GetMax() const;
		void Draw();
		void AddListener(function<void(int)> listener);
		void OnMouseClick(int x, int y);
};
#endif
