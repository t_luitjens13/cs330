#include "Widget.h"
#include "Label.h"
#include <vector>
#include <functional>
#ifndef WINDOW_H
#define WINDOW_H
using namespace std;

class Window : Widget{
	private:
		vector<Widget*> widgetList;
		bool stop;
		MEVENT event;
	public:
		Window() : Widget(0,0,0,0){
			stop = false;
			Widget::SetLeft(-1);
			Widget::SetRight(-1);
			Widget::SetTop(-1);
			Widget::SetBottom(-1);
			initscr();
			noecho();
			curs_set(0);
			keypad(stdscr, true); //window* , bool
			mousemask(BUTTON1_PRESSED, NULL); //mmask_t, mmask_t*
		}
		~Window(){
			for(Widget* widget : widgetList){
				delete widget;
			}
		}
		void Loop();
		void Add(Widget* widget);
		void Draw();
		void Stop();
		
};

#endif
