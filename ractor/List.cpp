#include "List.h"

int List::GetSelectedIndex() const{
	return index;
}
const string& List::GetSelected() const{
	if(index == -1)
		return NULL;
	else	
		return options.at(index);
}
void List::Draw(){
	
	for(int i=0;i<options.size();i++){
		if(i==GetSelectedIndex()){
			attron(A_REVERSE);
			mvprintw( y+i,x, "%s\n", options.at(i).c_str());
			attroff(A_REVERSE);
		}
		else
		mvprintw( y+i,x, "%s\n", options.at(i).c_str());
	}
	
	
}
void List::AddListener(function<void(int)> listener){
	listeners.push_back(listener);
}

void List::OnMouseClick(int xCoord, int yCoord){
	if(index == yCoord)
		index = -1;
	else
		index = yCoord;
	for(function<void(int)> listener : listeners){
		listener(index);
	}
}
