#include "Slider.h"

int Slider::GetValue() const{
	return currentVal;
}

int Slider::GetMax() const{
	return max;
}

void Slider::Draw(){
	for (int i = 0; i <= max; i++){
		mvprintw( y,i+x, "%s", ".");
	}
	mvprintw(y, x+currentVal, "%s", "o"); 
}

void Slider::AddListener(function<void(int)> listener){
	listeners.push_back(listener);
}

void Slider::OnMouseClick(int xCoord, int yCoord){
	
	currentVal = xCoord ;
	for(function<void(int)> listener : listeners){
		listener(currentVal);
	}
}
