#include "Rectangle.h"
#include <ncurses.h>
#include <iostream>
using namespace std;

Rectangle::Rectangle(int l, int r, int t, int b){
	left = l;
	right = r;
	top = t;
	bottom = b;
}

int Rectangle::GetLeft() const{
	return left;
}

int Rectangle::GetRight() const{
	return right;
}

int Rectangle::GetTop() const{
	return top;
}

int Rectangle::GetBottom() const{
	return bottom;
}

int Rectangle::GetHeight() const{
	return bottom-top + 1;
}

int Rectangle::GetWidth() const{
	return right-left + 1;
}

void Rectangle::SetLeft(int val){
	left = val;
}

void Rectangle::SetRight(int val){
	right = val;
}

void Rectangle::SetTop(int val){
	top = val;
}

void Rectangle::SetBottom(int val){
	bottom = val;
}

bool Rectangle::Contains(int x, int y) const{
	if(x <= right && x >= left && y >= top && y <= bottom)
		return true;
	return false;
}

bool Rectangle::Intersects(const Rectangle& rect){
	//returns true if any part of the rectangles intersect. Solve inverse first
	int l = rect.GetLeft();
	int r = rect.GetRight();
	int t = rect.GetTop();
	int b = rect.GetBottom();
	bool intersects = false;
	if(left <= l && l <= right && top <= t && t <= bottom)
		intersects = true;
	else if (left <= r && r <= right && top <= b && b<= bottom)
		intersects = true;
	else if (left <= l && l <= right && top <= b && b<= bottom)
		intersects = true;
	else if (left <= r && r <= right && top <= t && t <= bottom)
		intersects = true;
	return intersects;
}


