#include "Widget.h"
#include <functional>
#include <vector>
#ifndef BUTTON_H
#define BUTTON_H

using namespace std;

class Button : public Widget{
	private:
		int x;
		int y;
		string text;
		vector<function<void()>> listeners;
	public:
		Button(int xCoord, int yCoord, const string& intext): Widget(0,0,0,0){
			x = xCoord;
			y = yCoord;
			text = intext;
			Widget::SetLeft(xCoord);
			Widget::SetTop(yCoord);
			Widget::SetBottom(yCoord);
			Widget::SetRight(intext.length()-1+xCoord);
			
		}
		void Draw();
		void AddListener(function<void()> listener); //this is supposed to accept a listener of type std::function<void()>
		void OnMouseClick(int x, int y);		
};


#endif
