#ifndef WIDGET_H
#define WIDGET_H
#include <ncurses.h>
#include <iostream>
#include <stdlib.h>
#include <string>
class Widget{
	protected:
		void SetRight(int x);
		void SetLeft(int x);
		void SetTop(int x);
		void SetBottom(int x);
		
	private:
		int left;
		int right;
		int top;
		int bottom;
	public:
		Widget(int left, int right, int top, int bottom);
		virtual ~Widget();
		int GetLeft() const;
		int GetRight() const;
		int GetTop() const;
		int GetBottom() const;
		bool Contains(int x, int y) const;
		virtual void Draw() = 0;//the 0 makes this pure and therefore requires it to be implemented by the children
		virtual void OnMouseClick(int x, int y);
};
#endif //WIDGET_H
