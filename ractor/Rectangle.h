
class Rectangle{
	private:
		int left;
		int right;
		int top;
		int bottom; 
	public:
		Rectangle(int l, int r, int t, int b);
		int GetLeft() const;
		int GetRight() const;
		int GetTop() const;
		int GetBottom() const;
		void SetLeft(int l);
		void SetRight(int r);
		void SetTop(int t);
		void SetBottom(int b);
		int GetWidth() const;
		int GetHeight() const;
		bool Contains(int x, int y) const;
		bool Intersects(const Rectangle& rect);

};
