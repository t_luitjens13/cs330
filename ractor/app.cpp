using namespace std;
#include "Rectangle.h"
#include "Button.h"
#include "List.h"
#include "Checkbox.h"
#include "Slider.h"
#include "Label.h"
#include "Window.h"
int main(){
	
	//have a slider control the quote
	//populate lists
	//add everything to window
	
	Window window;
	
	Label* mon = new Label(10,0, "Monday");
	Label* tues = new Label(25,0, "Tuesday");
	Label* wed = new Label(40,0, "Wednesday");
	Label* thurs = new Label(55,0, "Thursday");
	Label* fri = new Label(70,0, "Friday");
	Label* sat = new Label(85,0, "Saturday");
	Label* sun = new Label(100,0, "Sunday");
	Label* done = new Label(10,18, "");
	
	string quote0 = "It's one of my theories that when people give you advice, they're really just talking to themselves in the past. 			-Mark Epstein";
	string quote1 = "Make sure your worst enemy doesn't live between your two ears. -Laird Hamilton";
	string quote2 = "Let me never fall into the vulgar mistake of dreaming that I am persecuted whenever I am contradicted. - Ralph 		Waldo Emerson";
	string quote3 = "If you went back and fixed all the mistakes you've made, you erase yourself. - Louis C.K.";
	string quote4 = "Never discourage anyone who continually makes progress, no matter how slow. - Plato";
	string quote5 = "Too often we enjoy the comfort of opinion without the discomfort of thought. - John F. Kennedy";
	string quote6 = "Isn't it funny how day be day nothing changes, but when we look back, everything is different. - C.S. Lewis";
	
	Label* quote = new Label(10, 28, quote0);
	vector <Checkbox*> monList;
	vector <Checkbox*> tuesList;
	vector <Checkbox*> wedList;
	vector <Checkbox*> thursList;
	vector <Checkbox*> friList;
	vector <Checkbox*> satList;
	vector <Checkbox*> sunList;
	
	monList.push_back(new Checkbox(7, 2, "Clean room"));
	monList.push_back(new Checkbox(7, 3, "Work at 2"));
	tuesList.push_back(new Checkbox(22, 2, "Laundry Day"));
	thursList.push_back(new Checkbox(52, 2, "Study for finals"));
	thursList.push_back(new Checkbox(52, 3, "Continue with wasd"));
	thursList.push_back(new Checkbox(52, 4, "Meet up for physics"));
	
	Slider* slider = new Slider(5,25,6,0);
	slider->AddListener([&] (int i){
		int selected = slider->GetValue();
		if(selected ==0)
			quote->SetText(quote0);
		if(selected ==1)
			quote->SetText(quote1);
		if(selected ==2)
			quote->SetText(quote2);
		if(selected ==3)
			quote->SetText(quote3);
		if(selected ==4)
			quote->SetText(quote4);
		if(selected ==5)
			quote->SetText(quote5);
		if(selected ==6)
			quote->SetText(quote6);
	});
	Button* button = new Button(130,1, "Exit");
	button->AddListener([&]() {
		window.Stop();		
 	 });
	
	for(Checkbox* widget : monList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : monList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Monday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : tuesList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : tuesList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Tuesday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : wedList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : wedList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Wednesday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : thursList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : thursList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Thursday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : friList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : friList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Friday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : satList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : satList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Saturday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	for(Checkbox* widget : sunList){
		widget->AddListener([&] (bool ean){
			bool allChecked = true;
			for(Checkbox* box : sunList){
				if(!box->IsChecked()){
					allChecked = false;
					
				}
			}
			if(allChecked == true)
				done->SetText("Congrats you finished Sunday!");
			else
				done->SetText("");
		});
		window.Add(widget);
	}
	window.Add(slider);
	window.Add(button);
	window.Add(mon);
	window.Add(tues);
	window.Add(wed);
	window.Add(thurs);
	window.Add(fri);
	window.Add(sat);
	window.Add(sun);
	window.Add(quote);
	window.Add(done);	
	window.Loop();
	return 0;
}
