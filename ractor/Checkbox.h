#include "Widget.h"
#include <vector>
#include <functional>
#ifndef CHECKBOX_H
#define CHECKBOX_H
using namespace std;

class Checkbox : public Widget{
	private:
		int x;
		int y;
		string text;
		bool checked;
		vector<function<void(bool)>> listeners;
	public:
		Checkbox(int xCoord, int yCoord, const string& intext) : Widget(0,0,0,0){
			x = xCoord;
			y = yCoord;
			text = intext;
			checked = false;
			Widget::SetLeft(xCoord);
			Widget::SetTop(yCoord);
			Widget::SetBottom(yCoord);
			Widget::SetRight(intext.size()+xCoord+3); // off by 3 for some reason
		}	
		bool IsChecked();
		void IsChecked(bool check);
		void Draw();
		void AddListener(function<void(bool)> listener);
		void OnMouseClick(int x, int y);
};

#endif
