module Utilities where
import Data.List(foldl', scanl', group)
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)


froto :: [a] -> [(a,a)]
froto (first : second : rest) = (first, second) : froto rest
froto [] = []
froto (first) = []



frotoByNearness :: [Integer] -> [(Integer, Integer)]
frotoByNearness = froto . sortWith nearness


sortWith :: [Integer] -> [Integer]
sortWith [] = []
sortWith(head : secondary : rest)
	
sortWith (head : rest) = []
 
