Soundary by Zach Schlieper and Tyler Luitjens

Program 
	: statement(s) 

Statement 
	: sound param 
	| if param statement end 
	| if param statement else param statement end 
	| print param 
	| while param statement end 
	| VAR leftParanthesis param rightParanthesis 
	| funct VAR leftParanthesis param rightParanthesis 
	|extend param param //used to lengthen soundbytes 
	|truncate param param //used ot shorten soundbytes 
	|truncate param param param //given a start and end time 
	| combine param param //combine 2 sounds together 
	| play param //play a sound 
	| write param param //save a sound/something else to a given file name

param
	: int 
	| string 
	| param (plus | mult | divide | mod | minus) param 
	| param (lessThan | greaterThan | lessThanEqualTo | greaterThanEqualTo) param

Soundary is a language that will allow for the playing of sounds with conditionals and loops. It will be Java-based and support sounds imported by the user. The interpreter will be fed a text file with Soundary code and will execute the contents of the file. 
