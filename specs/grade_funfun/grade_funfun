#!/usr/bin/env zsh

# ---------------------------------------------------------------------------- 

# Assert that each file has the required tokens.
function check_tokens() {
  file=$1
  shift
  for i in $*; do
    grep -- $i $file >/dev/null
    if [[ $? -ne 0 ]]; then
      echo "Yikes! Expected $i in $file not found." >& 2
      exit 1
    fi
  done
}

# ---------------------------------------------------------------------------- 

iskey=0
isgrader=0
onlylater=0
while getopts kgl opt; do
  case $opt in
    (k)
      iskey=1
      ;;
    (g)
      isgrader=1
      ;;
    (l)
      onlylater=1
      ;;
  esac
done

graderdir=${0:A:h}
srcdir=$(pwd)
libdir=$srcdir/../.tmp
basedir=${srcdir:t}
tmp="${srcdir}/.tmp"
name="funfun"
course=cs330
semester=2018a
actual_version=1

# Assert version
read expected_version < =(curl -s "https://twodee.org/teaching/vspec.php?course=$course&semester=$semester&homework=$name")
if [[ $expected_version != $actual_version ]]; then
  echo "Your SpecChecker appears to be out of date. Follow the directions "
  echo "in homework 0, part 3 to synchronize it."
  echo
  exit 1
fi

mkdir -p $libdir/to_trash

# Assert that we're in the right directory.
if [[ $basedir != $name ]]; then
  echo "Oh no! The grader must be run from the $name directory." >&2
  exit 1
fi

# ---------------------------------------------------------------------------- 

# Assert that all required files exist.
expected_files=(Utilities.hs)
for i in $expected_files; do
  if [[ ! -f $i ]]; then
    echo "Whoops! Expected file $i not found." >&2
    exit 1
  fi
done

echo "--------------------------------------------------------"
echo "Testing Utilities..."
runhaskell $graderdir/TestUtilities.hs
oktester=$?
if [[ $oktester -ne 0 ]]; then
  echo "--------------------------------------------------------"
  echo
  exit 1
fi

echo "--------------------------------------------------------"
echo "Testing Intervals..."
runhaskell $graderdir/TestIntervals.hs
oktester=$?
if [[ $oktester -ne 0 ]]; then
  echo "--------------------------------------------------------"
  echo
  exit 2
fi

echo "--------------------------------------------------------"
echo "Testing MindReader functions..."
runhaskell $graderdir/TestMindReader.hs
oktester=$?
if [[ $oktester -ne 0 ]]; then
  echo "--------------------------------------------------------"
  echo
  exit 2
fi

echo "--------------------------------------------------------"
echo "Testing MindReader I/O..."

for i in 31 40 23 8 56; do
  expected_file=$graderdir/cases/mr$i.expected
  actual_file=$libdir/to_trash/mr$i.actual
  runhaskell MindReader.hs < $graderdir/cases/mr$i.in > $actual_file
  
  if [[ $iskey -eq 1 ]]; then
    \cp $actual_file $expected_file
  fi

  diff $expected_file $actual_file >/dev/null
  if [[ $? -ne 0 ]]; then
    diff $expected_file $actual_file | grep "No newline at end of file" >/dev/null
    if [[ $? -eq 0 ]]; then
      echo "Ack! Output from MindReader doesn't end in a newline." >&2
    else
      echo "Ughh. Differences were detected when running a MindReader game with maximum $i."
      if [[ $isgrader -eq 0 ]]; then
        echo -n "Run vimdiff? (Hit :qa to quit.) [y] or n: "
        read answer
        if [[ "$answer" != "n" ]]; then
          vimdiff -b -R $expected_file $actual_file
        fi
      fi
    fi
    echo "--------------------------------------------------------"
    echo
    exit 2
  fi
done

echo "--------------------------------------------------------"
echo "Testing BullsAndCows functions..."
runhaskell $graderdir/TestBullsAndCows.hs
oktester=$?
if [[ $oktester -ne 0 ]]; then
  echo "--------------------------------------------------------"
  echo
  exit 2
fi

echo "--------------------------------------------------------"
echo "Testing BullsAndCows I/O..."

for i in 547 43 7429; do
  expected_file=$graderdir/cases/bc$i.expected
  actual_file=$libdir/to_trash/bc$i.actual
  runhaskell $graderdir/BullsAndCowsWrapper.hs $i < $graderdir/cases/bc$i.in > $actual_file
  
  if [[ $iskey -eq 1 ]]; then
    \cp $actual_file $expected_file
  fi

  diff $expected_file $actual_file >/dev/null
  if [[ $? -ne 0 ]]; then
    diff $expected_file $actual_file | grep "No newline at end of file" >/dev/null
    if [[ $? -eq 0 ]]; then
      echo "Ack! Output from BullsAndCows doesn't end in a newline." >&2
    else
      echo "Ughh. Differences were detected when running a BullsAndCows game with target $i."
      if [[ $isgrader -eq 0 ]]; then
        echo -n "Run vimdiff? (Hit :qa to quit.) [y] or n: "
        read answer
        if [[ "$answer" != "n" ]]; then
          vimdiff -b -R $expected_file $actual_file
        fi  
      fi
    fi
    echo "--------------------------------------------------------"
    echo
    exit 2
  fi
done

if [[ $isgrader -eq 0 ]]; then
  echo
  echo "We're about to test a two-digit game of BullsAndCows. Play until you win."
  echo -n "Are you ready? y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    echo "--------------------------------------------------------"
    echo
    exit 2
  fi
  echo
  runhaskell BullsAndCows.hs 2

  echo
  echo -n "Did the game play as specified? y or [n]? "
  read answer
  if [[ "$answer" != "y" ]]; then
    echo "--------------------------------------------------------"
    echo
    exit 2
  fi
  echo
fi

echo "--------------------------------------------------------"
echo

# Don't need manual checks for key. Bail.
if [[ $iskey -eq 1 ]]; then
  exit 0
fi

# Assert that a commit and push has happened or is about to happen.
if [[ $isgrader -eq 0 ]]; then
  echo -n "Did you use composition and point-free style where specified? y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi

  echo "Have you added any unadded files, committed, and pushed to Bitbucket?"
  echo -n "Run \"git status\" if you don't know. y or [n]? "
  read answer
  echo
  if [[ "$answer" != "y" ]]; then
    exit 2
  fi
fi

exit 0
