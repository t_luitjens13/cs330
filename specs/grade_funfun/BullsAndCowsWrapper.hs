import System.Environment
import BullsAndCows(game)

main :: IO ()
main = do
  args <- getArgs
  let target = head args
  game target
