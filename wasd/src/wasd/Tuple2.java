package wasd;

public class Tuple2<T,U>{
	private T first;
	private U second;
	public Tuple2(T t, U u){
		first=t;
		second=u;
	}

public T getFirst() {
	return first;
}

public U getSecond() {
		return  second;
}
}
//I might need to change the return and casting signatures on the getters