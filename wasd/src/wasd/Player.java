package wasd;

public class Player {
	private World world;
	private double fov;
	private Vector2 playerPos, lookAt;
	public Player(World world, double fov) {
		this.world = world;
		this.fov=fov;
		//TODO it initializes the player to be situated at the world's starting position and looking along the world's
		//lookat position
		playerPos = world.getStart();
		lookAt = world.getLookAt();
	}
	public double getFieldOfView() {
		return fov;
	}
	public Vector2 getPosition() {
		return playerPos;
	}
	public Vector2 getLookAt() {
		return lookAt;
	}
	public Vector2 getRight() {
		return lookAt.rotate(-90);
	}
	public void rotate(double degrees) {
		lookAt = lookAt.rotate(degrees);
	}
	public void advance(double dist) {
		//moves the player the specified distance along the lookat vector
		//first check to make sure the next spot is a passage, otherwise player stops
		//lookat vector is not modified
		//probably do a vector2 addition here by combining the values of the lookat vector and the
		//current position?
	}
	public void strafe(double dist) {
		//moves the player by the specified distance along the right vector
		//check for passage vs wall again first
		//lookat vector is not modified
	}
	
}
