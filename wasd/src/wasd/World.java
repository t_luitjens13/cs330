package wasd;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class World {
	private File file;
	private double  playerX, playerZ,lookAtX, lookAtZ;
	private int width, height;
	private int[][] map;
	public World(File file) throws FileNotFoundException {
		this.file = file;
		Scanner scan = new Scanner(this.file);
		width = scan.nextInt();
		height = scan.nextInt();
		map = new int[width][height];
		playerX = scan.nextDouble();
		playerZ = scan.nextDouble();
		lookAtX = scan.nextDouble();
		lookAtZ = scan.nextDouble();
		int i=0;
		int j = 0;
		while (scan.hasNextInt()) {
			map[i][j]=scan.nextInt();
			if(i==(width-1)) {
				i=0;
				j++;
			}
			else
			i++;
		}
		scan.close();
	}

	public Vector2 getStart() {
		return new Vector2(playerX, playerZ);
	}
	
	public Vector2 getLookAt() {
		return new Vector2(lookAtX, lookAtZ);
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public int get(int x, int z) {
		return map[x][z];
	}
	
	public boolean isPassage(int x, int z) {
		if(x<0||x>width-1)
			return false;
		else if(z<0||z>height-1)
			return false;
		return map[x][z] == 0;
	}
	
	public boolean isPassage(Vector2 vector) {
		int x = (int) vector.getX();
		int z = (int) vector.getY();
		if(x<0||x>width)
			return false;
		else if(z<0||z>height)
			return false;
		return map[(int)vector.getX()][(int)vector.getY()] == 0;
	}
	
	@SuppressWarnings({ "rawtypes", "unused" })
	public Tuple2 getDistanceToWall(Vector2 location, Vector2 direction) {
		int mapX = (int) location.getX();
		int mapY = (int) location.getY();
		 
		double rayDirX =  direction.getX() +0;
		double rayDirY =  (direction.getY()+.66);
		int sideDistX, sideDistY, stepX, stepY,side;
		int hit = 0;
		double perpWallDist;
		double deltaDistX = Math.abs(1/rayDirX);
		double deltaDistY =Math.abs(1/rayDirY);
		if(rayDirX < 0) {
			stepX = -1;
			sideDistX = (int) ((location.getX() - mapX) * deltaDistX);
		}
		else {
			stepX = 1;
			sideDistX = (int) ((mapX+1-location.getX()  ) * deltaDistX);
		}
		if(rayDirY < 0) {
			stepY=-1;
			sideDistY = (int) ((location.getY() - mapY) * deltaDistY);
		}
		else {
			stepY = 1;
			sideDistY = (int) ((mapY+1-location.getY()  ) * deltaDistY);

		}
		
		//perform DDA
		side = -1;
		while(hit==0) {
			if(sideDistX < sideDistY) {
				sideDistX += deltaDistX;
				mapX += stepX;
				side = 0;
			}
			else {
				sideDistY += deltaDistY;
				mapY += stepY;
				side = 1;
			}
			if(map[mapX][mapY] > 0)
				hit =1;
		}
		boolean wall = true;
		if(side==0) {
			perpWallDist = (mapX - (int) location.getX() + (1 - stepX) / 2) / rayDirX;
			wall = false;
			
		}
		else
			perpWallDist = (mapY - (int) location.getY() + (1 - stepY) / 2) / rayDirY;
		return new Tuple2<Double, Boolean>(perpWallDist, wall);
	}
	
}
