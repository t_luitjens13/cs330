package wasd;

public class Vector2 {
	private double xCoord, yCoord;
	public Vector2(double x, double y) {
		xCoord = x;
		yCoord = y;
	}
	
	public double getX() {
		return xCoord;
	}
	
	public double getY() {
		return yCoord;
	}
	
	public double length() {
		return Math.sqrt((xCoord*xCoord)+(yCoord*yCoord));
	}
	
	public Vector2 add(Vector2 vector) {
		return new Vector2(xCoord +vector.getX(), yCoord + vector.getY());
	}
	
	public Vector2 subtract(Vector2 vector) {
		return new Vector2(xCoord -vector.getX(), yCoord - vector.getY());
	}
	
	public Vector2 multiply(double scale) {
		return new Vector2(xCoord * scale, yCoord * scale);
	}
	
	public Vector2 divide(double scale) {
		return new Vector2(xCoord / scale, yCoord / scale);
	}
	
	public Vector2 normalize() {
		return new Vector2(xCoord*(1/this.length()), yCoord*(1/this.length()));
		//TODO not sure if this is right here
	}
	
	public Vector2 rotate(double degrees) {
		double xPrime = xCoord * Math.cos(Math.toRadians(degrees)) - yCoord * Math.sin(Math.toRadians(degrees)); 
		double yPrime = xCoord * Math.sin(Math.toRadians(degrees)) + yCoord * Math.cos(Math.toRadians(degrees)); 
		return new Vector2(xPrime, yPrime);
	}
	
	public String toString() {
		return String.format("%f", xCoord) + "," + String.format("%f", yCoord);
	}
}
